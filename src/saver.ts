console.log("init saver");

//let favs = [];

/*interface Produit {
    id: number,
    name: string,
    price: number,
    description: string,
    image: string,
    rating: number,
    numberNote: number,
    imgsDesc: string,
}*/

const fav = 'fav';

export function getFav(): number[] {
    const itemsJSON = localStorage.getItem(fav)
    let items: number[] = [];
    if (itemsJSON) {
        items = JSON.parse(itemsJSON) as number[];
    }
    return items;
}

export function addFav(itemId: number | null | undefined) {
    if (!itemId) return;
    const itemsJSON = localStorage.getItem(fav)
    let items = [];
    if (itemsJSON) {
        items = JSON.parse(itemsJSON);
    }
    items.push(itemId);
    localStorage.setItem(fav, JSON.stringify(items));
}

export function removeFav(itemId: number | null | undefined) {
    if (!itemId) return;
    const itemsJSON = localStorage.getItem(fav)
    let items: number[] = [];
    if (itemsJSON) {
        items = JSON.parse(itemsJSON) as number[];
        items = items.filter((item: number) => item !== itemId);

        localStorage.setItem(fav, JSON.stringify(items));
    }
}

export function removeAllFav() {
    localStorage.setItem(fav, JSON.stringify([]));
}

/*export function getFav(): Produit[] {
    const itemsJSON = localStorage.getItem('items')
    let items: Produit[] = [];
    if (itemsJSON) {
        items = JSON.parse(itemsJSON) as Produit[];
    }
    return items;
}

export function addFav(item: Produit) {
    const itemsJSON = localStorage.getItem('items')
    let items = [];
    if (itemsJSON) {
        items = JSON.parse(itemsJSON);
    }
    items.add(item);
    localStorage.setItem('items', JSON.stringify(items));
}*/

const pan = "pan";

interface ProduitPan {
    id: number,
    qte: number
}

export function getPan(): ProduitPan[] {
    const itemsJSON = localStorage.getItem(pan)
    let items: ProduitPan[] = [];
    if (itemsJSON) {
        items = JSON.parse(itemsJSON) as ProduitPan[];
    }
    return items;
}

export function addPan(itemId: number | null | undefined) {
    if (!itemId) return;
    const itemsJSON = localStorage.getItem(pan)
    let items = [];
    if (itemsJSON) {
        items = JSON.parse(itemsJSON);
    }
    const item = items.find((item: ProduitPan) => item.id === itemId);
    if (item) {
        items = items.map((item: ProduitPan) => {
            if (item.id === itemId) {
                return {
                    ...item,
                    qte: item.qte + 1
                }
            } else {
                return item;
            }
        })
    } else {
        items.push({
            id: itemId,
            qte: 1
        });
    }
    localStorage.setItem(pan, JSON.stringify(items));
}

export function upPan(itemId: number | null | undefined) {
    if (!itemId) return;
    const itemsJSON = localStorage.getItem(pan)
    let items = [];
    if (itemsJSON) {
        items = JSON.parse(itemsJSON);

        const item = items.find((item: ProduitPan) => item.id === itemId);
        if (item) {
            items = items.map((item: ProduitPan) => {
                if (item.id === itemId) {
                    return {
                        ...item,
                        qte: item.qte + 1
                    }
                } else {
                    return item;
                }
            })
        }

        localStorage.setItem(pan, JSON.stringify(items));
    }
}

export function downPan(itemId: number | null | undefined) {
    if (!itemId) return;
    const itemsJSON = localStorage.getItem(pan)
    let items = [];
    if (itemsJSON) {
        items = JSON.parse(itemsJSON);

        const item = items.find((item: ProduitPan) => item.id === itemId);
        if (item) {
            items = items.map((item: ProduitPan) => {
                if (item.id === itemId) {
                    return {
                        ...item,
                        qte: item.qte - 1
                    }
                } else {
                    return item;
                }
            })
        }

        localStorage.setItem(pan, JSON.stringify(items));
    }
}

export function removePan(itemId: number | null | undefined) {
    if (!itemId) return;
    const itemsJSON = localStorage.getItem(pan)
    let items: ProduitPan[] = [];
    if (itemsJSON) {
        items = JSON.parse(itemsJSON) as ProduitPan[];
        items = items.filter((item: ProduitPan) => item.id !== itemId);

        localStorage.setItem(pan, JSON.stringify(items));
    }
}

export function removeAllPan() {
    localStorage.setItem(pan, JSON.stringify([]));
}