import { mount } from '@vue/test-utils'
import HomeView from '@/views/HomeView.vue'

describe('HomeView.vue', () => {
  it('renders tab 1 HomeView', () => {
    const wrapper = mount(HomeView)
    expect(wrapper.text()).toMatch('Tab 1 page')
  })
})
